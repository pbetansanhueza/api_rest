const express    = require('express');
const app        = express();
const morgan     = require('morgan');
const bodyParser = require('body-parser');


// config
app.set('port', process.env.PORT || 9000);

//wares
app.use(morgan('dev'));
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

// rutas
require('./routes/usuariosRutas')(app);

app.listen(app.get('port'), () => {

	console.log('Server on port 9000');
});