const Usuarios = require('../models/usuarios');

module.exports = function(app){

	app.post('/users', (req, res) => {

		Usuarios.getUsuarios( (err,rows) => {

			res.status(200).json(rows);

		});

	});

	app.post('/insertarUsuarios', (req, res) => {

		const usuarioData = {
			first_name : req.body.first_name,
			last_name  : req.body.last_name,
			email      : req.body.email,
			password   : req.body.password,
			Username   : req.body.username
		}

		Usuarios.insertarUsuarios(usuarioData, (err, data) => {

			if(data && data.insertId){

				res.status(200).json({
					success : true,
					msg     : 'Usuario Insertado',
					data    : data
				});
			}
			else{

				res.status(500).json({
					success : false,
					msg     : err
				});

			}

		});

	});


	app.post('/modificarUsuarios', (req, res) => {

		const usuarioData = {
			first_name : req.body.first_name,
			last_name  : req.body.last_name,
			email      : req.body.email,
			password   : req.body.password,
			username   : req.body.username,
			id         : req.body.id
		}

		Usuarios.modificarUsuarios(usuarioData, (err, data) => {

			if(data.success){

				res.status(200).json({
					data    : data,
					status  : 200
				});
			}
			else{

				res.status(500).json({
					data    : data,
					status  : 500
				});

			}

		});

	});


	app.post('/eliminarUsuario', (req, res) => {

		const usuarioData = {
			id : req.body.id
		}

		Usuarios.eliminarUsuario(usuarioData, (err, data) => {

			if(data.success){

				res.status(200).json({
					data    : data
				});
			}
			else{

				res.status(500).json({
					data    : data
				});

			}

		});

	});
	app.post('/login', (req, res) => {
		
				const usuarioData = {
					username : req.body.email,
					password: req.body.password
				}
		
				Usuarios.login(usuarioData, (err, data) => {
		
					if(data.success){
		
						res.status(200).json({
							data    : data
						});
					}
					else{
		
						res.status(500).json({
							data    : data
						});
		
					}
		
				});
		
			});

}