const mysql = require('mysql');

connection  = mysql.createConnection({

	host: 'localhost',
	user: 'root',
	password: '',
	database: 'snuuper_test'

});

let usuariosModel = {};

usuariosModel.getUsuarios = (callback) => {

	if(connection){
		connection.query('select * from mock_data', (err,rows) => {

			if(err){
				callback(null, {
					'msg' : err
				});
			}
			else{
				callback(null, {'success':'true', data :rows });
			}
		});
	}
};

usuariosModel.insertarUsuarios = (usuarioData, callback) => {

	if(connection){
		connection.query('insert into mock_data set ?', usuarioData, 

		(err, result) => {

			if(err){
				callback(err,null)
			}
			else{
				callback(null, {

					'insertId' : result.insertId
				});
			}
		});
	}
}

usuariosModel.modificarUsuarios = (usuarioData, callback) => {

	if(connection){

		sql       = "update mock_data SET "; 
		sql      += "first_name = '"+usuarioData.first_name+"' ,";
		sql      += "last_name  = '"+usuarioData.first_name+"' ,";
		sql      += "email      = '"+usuarioData.email+"' ,";
		sql      += "password   = '"+usuarioData.password+"' ,";
		sql      += "Username   = '"+usuarioData.username+"' ";
		sql      += " where id  = "+usuarioData.id+" ";


		connection.query(sql, (usuarioData), 
		(err, result) => {
			if(err || !result.changedRows){

				callback(null, {
					'msg'     : result.message,
					'success' : false
				});
			}
			else{
				callback(null, {
					'msg'     : result.message,
					'success' : true
				});
			}
		});
	}
}


usuariosModel.eliminarUsuario = (usuarioData, callback) => {

	if(connection){

		sql       = " delete from mock_data "; 
		sql      += " where id  = '" + usuarioData.id + "'";


		connection.query(sql, (usuarioData), 

		(err, result) => {
			console.log(result);

			if(err || !result.affectedRows){

				callback(null, {
					'msg'     : 'No existe este id',
					'success' : false
				});
			}
			else{
				callback(null, {
					'msg'     : 'Eliminado con éxito',
					'success' : true
				});
			}
		});
	}
}

usuariosModel.login = (usuarioData, callback) => {
	
		if(connection){
	
			sql       = " select * from mock_data "; 
			sql      += " where username  = '" +usuarioData.username+ "'";
	
	
			connection.query(sql, (usuarioData), 
	
			(err, result) => {
				console.log(result);
	
				if(err || result.length<=0){
					console.log(err);
	
					callback(null, {
						'msg'     : 'No existe este username',
						'success' : false
					});
				}
				else{
					if(result[0].password == usuarioData.password)
					callback(null, {
						'msg'     : 'login success',
						'success' : true
					});
					else
					callback(null, {
						'msg'     : 'Password Mismatch',
						'success' : false
					});
				}
			});
		}
	}


module.exports = usuariosModel;